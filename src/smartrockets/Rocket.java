package smartrockets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Rocket {
	
	private Vector pos;
	private Vector vel;
	private Vector acc;
	private DNA dna;
	private double fitness;
	private boolean isSuccessful;
//	BufferedImage img;
	
	public Rocket() throws IOException {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		pos = new Vector(Screen.WIDTH / 2, Screen.HEIGHT * 7/8);
		vel = new Vector(0, 0);
		acc = new Vector(0, 0);
		dna = new DNA(300);
		isSuccessful = false;
//		img = ImageIO.read(getClass().getResource("/smartrockets/img_88.png"));
	}
	
	public Rocket(DNA newDNA) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		pos = new Vector(Screen.WIDTH / 2, Screen.HEIGHT * 7/8);
		vel = new Vector(0, 0);
		acc = new Vector(0, 0);
		dna = newDNA;
	}
	
	public DNA getDNA() {
		return dna;
	}
	
	public void applyForce(Vector other) {
		acc.add(other);
	}
	
	/**
	* Updates the rocket with its new position and velocity for the 
	* next frame
	*
	* @author Daniel Zhu
	*/
	public void update() {
		vel.add(acc);
		pos.add(vel);
		acc.mult(0);
	}
	
	public boolean hitEdge() {
		return pos.getX() > Screen.WIDTH - 12 || pos.getX() < 2 || pos.getY() > Screen.HEIGHT - 35 || pos.getY() < 2;
	}
	
	/**
	* Checks if the rocket is successful.
	*
	* @author Daniel Zhu
	* @return a boolean with the result
	*/
	public boolean hitTarget() {
		isSuccessful = Vector.dist(pos, Screen.getTarget()) < 5;
		return isSuccessful;
	}
	
	/**
	* Checks if the rocket hit any of the obstacles
	*
	* @author Nour Ayad
	* @return a boolean with the result.
	*/
	public boolean hitObstacle(ArrayList<Obstacle> obstacles){
		for(Obstacle x: obstacles){
			if(pos.getY() > x.getTop() && pos.getY() < x.getBottom()){
				if(pos.getX() > x.getLeft() && pos.getX() < x.getRight()){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	* Calculates fitness based on the distance from the target
	* If the rocket has reached the target, fitness is based on steps
	* it took to reach the target.
	*
	* @author Daniel Zhu
	*/
	public void calculateFitness() {
		if (hitTarget()) {
			fitness = 1/10.0 + 10000.0/(dna.getStep() * dna.getStep());
		} else {
			double distance = Vector.dist(pos, Screen.getTarget());
			fitness = 1 / (distance * distance);
		}
	}
	
	public double getFitness() {
		return fitness;
	}
	
	public void setFitness(double fit) {
		fitness = fit;
	}
	
	public boolean getSuccess() {
		return isSuccessful;
	}
	
	public void draw(Graphics g) {
		g.setColor(Color.CYAN);
		g.fillRect((int)pos.getX(), (int)pos.getY(), 5, 5);
//		g.drawImage(img, (int)pos.getX(), (int)pos.getY(), null);
	}
}

