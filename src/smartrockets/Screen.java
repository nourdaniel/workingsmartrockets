package smartrockets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ImageObserver;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Screen extends JPanel implements MouseListener {
	
	public static final int HEIGHT = 600, WIDTH = 550;
	private static Vector target;
	private static int targetSize;
	private Rocket[] fleet;
	private ArrayList<Obstacle> obstacles;
	private ArrayList<Rocket> matingPool;
	private double mutationRate;
	private int tick;
	private int reachedTarget;
	private int maxReached;
	private int generation;
	private int speed;
	private boolean started;
	private boolean creating;
	private int x1,y1,x2,y2;
	private JButton button;
	
	public Screen(int n, JButton start) throws IOException {
		fleet = new Rocket[n];
		obstacles = new ArrayList<Obstacle>();
		started = false;
		for (int i = 0; i < fleet.length; i++) {
			fleet[i] = new Rocket();
		}
		target = new Vector(WIDTH/2, HEIGHT * 1/10);
		matingPool = new ArrayList<Rocket>();
		targetSize = 10;
		tick = 0;
		reachedTarget = 0;
		maxReached = 0;
		mutationRate = 0.01;
		speed = 1;
		button = start;
	}
	
	public static Vector getTarget() {
		return target;
	}
	
	public void changeSpeed(int delay) {
		switch(delay) {
		  case 20: speed = 1;
		           break;
		  case 10: speed = 2;
		           break;
		  case 5:  speed = 4;
		  		   break;
		  default: speed = 8;
		  		   break;
		}
	}
	
	/**
	* Selects 2 random & different rockets from the mating pool
	* Crosses them both over and mates them.
	*
	* @author Daniel Zhu
	* @return a new Rocket with the new DNA
	*/
	public Rocket selection() {
		int index = (int)(Math.random() * matingPool.size());
		DNA parentA = matingPool.get(index).getDNA();
		index = (int)(Math.random() * matingPool.size());
		DNA parentB = matingPool.get(index).getDNA();
		while (parentB.equals(parentA)) {
			index = (int)(Math.random() * matingPool.size());
			parentB = matingPool.get(index).getDNA();
		}
		DNA newDNA = parentA.crossOver(parentB);
		newDNA.mutate(mutationRate);
		return new Rocket(newDNA);
	}
	
	/**
	* Selects a random rocket that is successful and clones it
	*
	* @author Daniel Zhu
	* @return a new Rocket with the new DNA
	*/
	public Rocket clone() {
		int index = (int)(Math.random() * matingPool.size());
		Rocket parent = matingPool.get(index);
		while (!parent.getSuccess()) {
			index = (int)(Math.random() * matingPool.size());
			parent = matingPool.get(index);
		}
		DNA newDNA = parent.getDNA().copy();
		return new Rocket(newDNA);
	}
	
	/**
	* Draws the frame of the screen. This is automatically called
	* by the graphics engine at a set rate
	* 
	* @author Daniel Zhu & Nour Ayad
	* @param g is the graphics engine parameter
	*/
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		initializeScreen(g);
		if(!started){
			//This is the start phase. Simulation autostarts at 3 obstacles.
			drawObstacles(g);
			if(obstacles.size() > 3){
				started = true;
				button.setVisible(false);
			}
		}else {
			//After the generation reaches the max amount of steps, it resets
			if (tick == fleet[0].getDNA().getDirections().length) {
				calcMinStep();
				fillMatingPool();
				createNextGen();
				calcNumSuccessful();
				tick = 0;
				generation++;
			}
			drawObstacles(g);
			drawRockets(g);
			g.setColor(Color.WHITE);
			g.setFont(new Font("Verdana", Font.PLAIN, 18));
			g.drawString("Completion: " + Math.floor(maxReached/(double)fleet.length * 100) + "%", WIDTH - 190, HEIGHT - 40);
			g.drawString("Generation: " + generation, 20, HEIGHT - 40);
			g.drawString("Speed: " + speed + "x", WIDTH - 120, 30);
			tick++;
		}
	}

	private void calcNumSuccessful() {
		if (reachedTarget > maxReached) {
			maxReached = reachedTarget;
		}
		reachedTarget = 0;
	}

	/**
	* Creates the next generation of rockets based on 
	* whether the previous successes or failures of the
	* previous generation
	* 
	* @author Nour Ayad
	*/
	private void createNextGen() {
		for (int i = 0; i < fleet.length; i++) {
			if (fleet[i].getSuccess()) {
				fleet[i] = clone();
				reachedTarget++;
			} else {
				fleet[i] = selection();
			}
		}
	}

	public void drawRockets(Graphics g) {
		for (int i = 0; i < fleet.length; i++) {
			fleet[i].draw(g);
			if (!fleet[i].hitEdge() && !fleet[i].hitTarget() && !fleet[i].hitObstacle(obstacles)) {
				Vector force = fleet[i].getDNA().getForce();
				fleet[i].applyForce(force);
				fleet[i].update();
			}
		}
	}

	public void drawObstacles(Graphics g) {
		for(Obstacle x: obstacles){
			x.paint(g);
		}
	}

	/**
	* Calculates the rocket which reached the target fastest
	* and how fast that rocket was
	* @author Daniel Zhu
	*/
	public void calcMinStep() {
		int minStep = fleet[0].getDNA().getDirections().length;
		for (Rocket x: fleet) {
			if (x.hitTarget() && x.getDNA().getStep() < minStep) {
				minStep = x.getDNA().getStep();
			}
		}
		System.out.println("Steps: " + minStep);
	}

	/**
	* Fills the mating pool based on rocket fitness.
	* The more fit a rocket, the bigger the proportion
	* of the mating pool is made up of it.
	*
	* @author Nour Ayad
	*/
	public void fillMatingPool() {
		matingPool.clear();
		calcFleetFitness();
		for (int i = 0; i < fleet.length; i++) {
			double n = fleet[i].getFitness() * 100;
			for (int j = 0; j < n; j++) {
				matingPool.add(fleet[i]);
			}
		}
	}

	private void calcFleetFitness() {
		double maxFit = 0;
		for (int i = 0; i < fleet.length; i++) {
			fleet[i].calculateFitness();
			if(fleet[i].hitObstacle(obstacles)){
				fleet[i].setFitness(fleet[i].getFitness() * .3);
			}
			
			if (fleet[i].getFitness() > maxFit) {
				maxFit = fleet[i].getFitness();
			}
		}
		for (int i = 0; i < fleet.length; i++) {
			fleet[i].setFitness(fleet[i].getFitness() / maxFit);
		}
	}

	public void initializeScreen(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		g.setColor(Color.WHITE);
		g.setColor(Color.RED);
		g.fillOval((int)target.getX() - targetSize/2, (int)target.getY() - targetSize/2, targetSize, targetSize);
	}
	
	public void start(JButton button){
		started = true;
		button.setVisible(false);
	}
	
	public void createObstacle(){
		obstacles.add(new Obstacle(x1,y1,x2,y2));
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		if(!started){
			if(!creating){
				creating = true;
				x1 = e.getX();
				y1 = e.getY();
				System.out.println("lol");
			} else {
				creating = false;
				x2 = e.getX();
				y2 = e.getY();
				createObstacle();
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
		
