package smartrockets;

import java.awt.Color;
import java.awt.Graphics;

public class Obstacle {

	private Vector pos;
	private double width;
	private double length;
	
	public Obstacle(Vector p, double w, double l){
		pos = p;
		width = w;
		length = l;
	}
	
	public Obstacle(double x1, double y1, double x2, double y2){
		pos = new Vector(x1, y1);
		width = x2 - x1;
		length = y2 - y1;
	}
	
	public void paint(Graphics g){
		g.setColor(Color.BLUE);
		g.fillRect((int)pos.getX(), (int)pos.getY(), (int)width, (int)length);
	}
	
	public double getTop(){
		return pos.getY();
	}
	
	public double getBottom(){
		return pos.getY() + length;
	}
	
	public double getLeft(){
		return pos.getX();
	}
	
	public double getRight(){
		return pos.getX() + width;
	}
}
