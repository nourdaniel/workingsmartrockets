package smartrockets;

public class Vector {
	private double x;
	private double y;
	
	public Vector(double x1, double y1) {
		x = x1;
		y = y1;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public Vector set(double newX, double newY) {
		x = newX;
		y = newY;
		return this;
	}
	
	/**
	* Adds 2 vectors together
	*
	* @author Daniel Zhu
	* @param other is the other vector to add
	* @return this current vector after the addition
	*/
	public Vector add(Vector other) {
		x += other.getX();
		y += other.getY();
		return this;
	}
	
	/**
	* Multiplies 2 vectors together
	*
	* @author Daniel Zhu
	* @param other is the other vector to add
	* @return this current vector after the multiplication
	*/
	public Vector mult(double n) {
		x *= n;
		y *= n;
		return this;
	}
	
	public static double dist(Vector v1, Vector v2) {
	    double dx = v1.x - v2.x;
	    double dy = v1.y - v2.y;
	    return (double) Math.sqrt(dx * dx + dy * dy);
	}
	
	public String toString() {
		return "" + getX() + ", " + getY();
	}

}
