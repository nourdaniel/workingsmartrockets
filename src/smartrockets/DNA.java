package smartrockets;


public class DNA {
	private Vector[] dna;
	private int step;
	private static final double scaleForce = .2;
	
	public DNA(int size) {
		dna = new Vector[size];
		randomize();
		step = -1;
	}
	
	public DNA(Vector[] newDNA) {
		dna = newDNA;
		step = -1;
	}
	
	/**
	* Creates a set of DNA with random instructions
	*
	* @author Daniel Zhu
	*/
	private void randomize() {
		for(int i = 0; i < dna.length; i++) {
			double radians = Math.random() * Math.PI * 2;
			dna[i] = new Vector(Math.cos(radians), Math.sin(radians));
			dna[i].mult(scaleForce);
		}
	}
	
	public Vector[] getDirections() {
		return dna;
	}
	
	/**
	* Takes a piece of DNA and randomly selects a location to cross over
	* with another piece of DNA
	*
	* @author Daniel Zhu
	* @param other is the other piece of DNA to cross over with
	* @return a new set of the crossed over DNA
	*/
	public DNA crossOver(DNA other) {
		Vector[] newDNA = new Vector[dna.length];
		int index = (int)(Math.random() * dna.length);
		for (int i = 0; i < newDNA.length; i++) {
			if (i < index) {
				newDNA[i] = dna[i];
			} else {
				newDNA[i] = other.getDirections()[i];
			}
		}
		return new DNA(newDNA);
	}
	
	/**
	* Selects a random gene in the DNA and mutates it
	* at the given mutation rate
	*
	* @author Daniel Zhu
	* @param mRate is the rate at which DNA mutates
	*/
	public void mutate(double mRate) {
		for(int i = 0; i < dna.length; i++) {
			if (Math.random() < mRate) {
				double radians = Math.random() * Math.PI * 2;
				dna[i] = new Vector(Math.cos(radians), Math.sin(radians));
				dna[i].mult(scaleForce);
			}
		}
	}
	
	/**
	* Copies the current piece of DNA
	*
	* @author Daniel Zhu
	* @return a new set of identical DNA
	*/
	public DNA copy() {
		DNA newDNA = new DNA(dna.length);
		for (int i = 0; i < dna.length; i++) {
			newDNA.getDirections()[i] = dna[i];
		}
		return newDNA;
	}
	
	/**
	* returns the current instruction in the DNA, and increments
	* to the next gene
	*
	* @author Daniel Zhu
	* @return a vector with the force outlined in the DNA
	*/
	public Vector getForce() {
		if (step < dna.length) {
			return dna[incrementStep()];
		} 
		System.out.println(step);
		return null;
	}
	
	private int incrementStep() {
		step++;
		return step;
	}
	
	public int getStep() {
		return step;
	}
	
}

