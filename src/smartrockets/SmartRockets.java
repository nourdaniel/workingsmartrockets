package smartrockets;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Timer;

public class SmartRockets implements ActionListener {
	
	public static final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	private JFrame frame;
	private Screen screen;
	private Timer timer = new Timer(20, this);
	
	public SmartRockets() throws IOException {
		frame = new JFrame("Smart Rockets");
		frame.setResizable(false);
		frame.setSize(Screen.WIDTH, Screen.HEIGHT);
		frame.setLocation(dim.width/2 - frame.getWidth()/2, dim.height/2 
				- frame.getHeight()/2);
		
		JButton start = new JButton("Start");
		frame.add(screen = new Screen(100, start));
		frame.addMouseListener(screen);
		JButton fast = new JButton("Fast Forward");
		screen.add(fast);
		
		screen.add(start);
		
		fast.setSize(new Dimension(120, 30));
		fast.setLocation(new Point(20, 10));
		fast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTimer();
				screen.changeSpeed(timer.getDelay());
			}
		});
		start.setSize(new Dimension(120, 30));
		start.setLocation(new Point(100, 100));
		start.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				screen.start(start);
			}
		});
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		timer.start();
		frame.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		screen.repaint();
	}
	
	public void setTimer() {
		if (timer.getDelay() <= 2.5) {
			timer.setDelay(20);
		} else {
			timer.setDelay(timer.getDelay() / 2);
		}
	}
	
	public static void main(String[] args) throws IOException {
		new SmartRockets();
	}
}
